<?php
/*
 * Copyright 2013 Florian Plattner http://florianplattner.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



define( 'FOLDER' , dirname(dirname(__FILE__)));

require_once dirname(FOLDER) . DIRECTORY_SEPARATOR . 'image-cache/downloaders/StreamDownloader.php';


class StreamDownloaderTest extends PHPUnit_Framework_TestCase {


    public function testDownload() {
        $downloader = new StreamDownloader();

        $source = 'https://bitbucket.org/fplattner/image-cache/raw/56f81b77bce1132c69d30c065849cc9d24aabf0a/Test/resources/cat.jpg';
        $target = FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'downloadTest.jpg';

        $result = $downloader->download($source, $target);
        $this->assertTrue($result);

        $this->assertFileExists($target);
        $this->assertFileEquals($target, FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'cat.jpg');

        if (file_exists($target)) {
            unlink($target);
        }

        // test non existing target
        $target = FOLDER . DIRECTORY_SEPARATOR . 'test' . DIRECTORY_SEPARATOR . 'invalidFolder.jpg';
        $result = $downloader->download($source, $target);
        $this->assertFalse($result);

        // test non existing source
        $target = FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'downloadTest.jpg';
        $source = 'http://florianplattner.de/nonexistent.jpg';
        $result = $downloader->download($source, $target);
        $this->assertFalse($result);
    }
}