<?php
/*
 * Copyright 2013 Florian Plattner http://florianplattner.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



define( 'FOLDER' , dirname(dirname(__FILE__)));

require_once dirname(FOLDER) . DIRECTORY_SEPARATOR . 'image-cache' . DIRECTORY_SEPARATOR . 'ImageCache.php';

/**
 *
 */
class ImageCacheHelper extends ImageCache {

    /**
     * @param $srcImageDimensions
     *
     * @return array
     */
    public function publicGetCropDimensions($srcImageDimensions) {
        return parent::getCropDimensions($srcImageDimensions);
    }

    /**
     * @param $srcImageDimension
     *
     * @return array
     */
    public function publicGetCachedImageDimensions($srcImageDimension) {
        return parent::getCachedImageDimensions($srcImageDimension);
    }

    /**
     * @param $url
     *
     * @return string
     */
    public function publicCreateCacheUrl($url) {
        return parent::createCacheUrl($url);
    }

    /**
     * @param $url
     *
     * @return string
     */
    public function publicCreateCacheFilename($url) {
        return parent::createCacheFilename($url);
    }
}


/**
 *
 */
class ImageCacheTest extends PHPUnit_Framework_TestCase {

    /** @var array */
    private $srcImageDimension;
    /** @var array */
    private $srcImageDimensionTall;
    /** @var ImageCacheHelper */
    private $imageCache;
    /** @var string */
    private $cacheDir;
    /** @var array */
    private $calcProportions;


    public function setUp() {
        $this->srcImageDimension = array(
            'width' => 600,
            'height' => 399,
        );

        $this->srcImageDimensionTall = array(
            'width'  => 232,
            'height' => 399,
        );

        $this->cacheDir = FOLDER . DIRECTORY_SEPARATOR . 'resources';

        $this->calcProportions = array(
            ImageCache::PROPORTION_CROP_CENTERED, ImageCache::PROPORTION_CROP_RANDOM,
            ImageCache::PROPORTION_KEEP_HEIGHT, ImageCache::PROPORTION_KEEP_WIDTH,
            ImageCache::PROPORTION_STRETCH,
        );

        $this->imageCache = new ImageCacheHelper($this->cacheDir);
    }

    public function testConstruction() {
        $this->assertEquals($this->cacheDir, $this->imageCache->getCachePath());
        $this->assertEquals('GDProcessor', $this->imageCache->getImageProcessorName());
        $this->assertInstanceOf('StreamDownloader', $this->imageCache->getImageDownloader());
        $this->assertEquals(0, $this->imageCache->getImageWidth());
        $this->assertEquals(0, $this->imageCache->getImageHeight());
        $this->assertEquals(ImageCache::PROPORTION_CROP_CENTERED, $this->imageCache->getCalcProportion());
    }


    public function testGetCachedImageDimensions() {

    }



    public function testGetCropDimensionsWidthHeightZero() {
        // testing width and height = 0, results for all calcProportions must be the original image dimensions

        // test a wide source
        $expectedResult = array(
            'top'    => 0,
            'left'   => 0,
            'width'  => 600,
            'height' => 399,
        );

        foreach ($this->calcProportions as $proportion) {
            $this->imageCache->setCalcProportion($proportion);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimension);
            $this->assertEquals($expectedResult, $result);
        }

        // test a tall source
        $expectedResult = array(
            'top'    => 0,
            'left'   => 0,
            'width'  => 232,
            'height' => 399,
        );

        foreach ($this->calcProportions as $proportion) {
            $this->imageCache->setCalcProportion($proportion);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimensionTall);
            $this->assertEquals($expectedResult, $result);
        }
    }

    public function testGetCropDimensionsWidthZero() {
        // testing width = 0, height = 150

        // test a wide source
        $expectedResult = array(
            'top'    => 0,
            'left'   => 0,
            'width'  => 600,
            'height' => 399,
        );

        $this->imageCache->setImageHeight(150);
        for ($index = 0; $index < count($this->calcProportions); $index += 1) {
            $this->imageCache->setCalcProportion($this->calcProportions[$index]);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimension);
            $this->assertEquals($expectedResult, $result);
            echo '<pre>';
            var_dump($expectedResult);
            echo '</pre>';
            
            echo '<pre>';
            var_dump($result);
            echo '</pre>';
        }

        // test a tall source
        $expectedResult = array(
            'top'    => 0,
            'left'   => 0,
            'width'  => 232,
            'height' => 399,
        );

        foreach ($this->calcProportions as $proportion) {
            $this->imageCache->setCalcProportion($proportion);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimensionTall);
            $this->assertEquals($expectedResult, $result);
        }
    }


    public function testGetCropDimensionsHeightZero() {
        // testing width = 100, height = 0

        // test a wide source
        $expectedResult = array(
            'top'    => 0,
            'left'   => 0,
            'width'  => 600,
            'height' => 399,
        );

        $this->imageCache->setImageWidth(100);
        for ($index = 0; $index < count($this->calcProportions); $index += 1) {
            $this->imageCache->setCalcProportion($this->calcProportions[$index]);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimension);
            $this->assertEquals($expectedResult, $result);
        }


        // test a tall source
        $expectedResult = array(
            'top'    => 0,
            'left'   => 0,
            'width'  => 232,
            'height' => 399,
        );

        foreach ($this->calcProportions as $proportion) {
            $this->imageCache->setCalcProportion($proportion);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimensionTall);
            $this->assertEquals($expectedResult, $result);
        }
    }


    public function testGetCropDimensionsWidthHeightNonZero() {
        //testing width = 100, height = 150

        // test a wide source
        $expectedResults = array(
            array( // result for PROPORTION_CROP_CENTERED
                'top'    => 0,
                'left'   => 167,
                'width'  => 266,
                'height' => 399,
            ),
            array( // result for PROPORTION_CROP_RANDOM
                'top'    => 0,
                'left'   => 0, // this is a random value between and including 0 and 167
                'width'  => 266,
                'height' => 399,
            ),
            array( // result for PROPORTION_KEEP_WIDTH
                'top'    => 0,
                'left'   => 0,
                'width'  => 600,
                'height' => 399,
            ),
            array( // result for PROPORTION_KEEP_HEIGHT
                'top'    => 0,
                'left'   => 0,
                'width'  => 600,
                'height' => 399,
            ),
            array( // result for PROPORTION_STRETCH
                'top'    => 0,
                'left'   => 0,
                'width'  => 600,
                'height' => 399,
            ),
        );

        $this->imageCache->setImageWidth(100);
        $this->imageCache->setImageHeight(150);
        for ($index = 0; $index < count($this->calcProportions); $index += 1) {
            $this->imageCache->setCalcProportion($this->calcProportions[$index]);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimension);
            // since PROPORTION_CROP_RANDOM generates random values, we run it several times to
            // increase the odds that the random value always is in the expected range.
            if (ImageCache::PROPORTION_CROP_RANDOM == $this->calcProportions[$index]) {
                for ($counter = 0; $counter < 1000; $counter += 1) {
                    $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimension);
                    $this->assertEquals($expectedResults[$index]['top'], $result['top']);
                    $this->assertGreaterThanOrEqual(0, $result['left']);
                    $this->assertLessThanOrEqual(167, $result['left']);
                    $this->assertEquals($expectedResults[$index]['width'], $result['width']);
                    $this->assertEquals($expectedResults[$index]['height'], $result['height']);
                }
            } else {
                $this->assertEquals($expectedResults[$index], $result);
            }
        }


        // test a tall source
        $expectedResults = array(
            array( // result for PROPORTION_CROP_CENTERED
                'top'    => 25,
                'left'   => 0,
                'width'  => 232,
                'height' => 348,
            ),
            array( // result for PROPORTION_CROP_RANDOM
                'top'    => 0, // this is a random value between and including 0 and 25
                'left'   => 0,
                'width'  => 232,
                'height' => 348,
            ),
            array( // result for PROPORTION_KEEP_WIDTH
                'top'    => 0,
                'left'   => 0,
                'width'  => 232,
                'height' => 399,
            ),
            array( // result for PROPORTION_KEEP_HEIGHT
                'top'    => 0,
                'left'   => 0,
                'width'  => 232,
                'height' => 399,
            ),
            array( // result for PROPORTION_STRETCH
                'top'    => 0,
                'left'   => 0,
                'width'  => 232,
                'height' => 399,
            ),
        );

        for ($index = 0; $index < count($this->calcProportions); $index += 1) {
            $this->imageCache->setCalcProportion($this->calcProportions[$index]);
            $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimension);
            // since PROPORTION_CROP_RANDOM generates random values, we run it several times to
            // increase the odds that the random value always is in the expected range.
            if (ImageCache::PROPORTION_CROP_RANDOM == $this->calcProportions[$index]) {
                for ($counter = 0; $counter < 1000; $counter += 1) {
                    $result = $this->imageCache->publicGetCropDimensions($this->srcImageDimensionTall);
                    $this->assertEquals($expectedResults[$index]['top'], $result['top']);
                    $this->assertGreaterThanOrEqual(0, $result['left']);
                    $this->assertLessThanOrEqual(25, $result['left']);
                    $this->assertEquals($expectedResults[$index]['width'], $result['width']);
                    $this->assertEquals($expectedResults[$index]['height'], $result['height']);
                }
            } else {
                $this->assertEquals($expectedResults[$index], $result);
            }
        }
    }

}