<?php
/*
 * Copyright 2013 Florian Plattner http://florianplattner.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define( 'FOLDER' , dirname(dirname(__FILE__)));

require_once dirname(FOLDER) . DIRECTORY_SEPARATOR . 'image-cache/processors/GDProcessor.php';


class GDProcessorTest extends PHPUnit_Framework_TestCase {

    protected $testImages;
    protected $testFileTypes;

    public function setUp() {
        $this->testImages = array(
            FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'cat.jpg',
            FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'cat.jpeg',
            FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'cat.png',
            FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'cat.gif',
        );

        $this->testFileTypes = array(
            'jpg',
            'jpg',
            'png',
            'gif',
        );
    }


    public function testConstruction() {
        // assert file types

        for ($index = 0; $index < count($this->testImages); $index += 1) {
            $processor = new GDProcessor($this->testImages[$index]);

            $this->assertEquals($processor->getFileType(), $this->testFileTypes[$index]);
            $this->assertInternalType('resource', $processor->getSrcImage());
            $this->assertEquals(600, imagesx($processor->getSrcImage()), _('Source image has not the expected width.'));
            $this->assertEquals(399, imagesy($processor->getSrcImage()), _('Source image has not the expected height'));

            $dimensions = $processor->getImageDimensions();
            $this->assertEquals(imagesx($processor->getSrcImage()), $dimensions['width'], _('Reported width and actual width of the source image don\'t match.'));
            $this->assertEquals(imagesy($processor->getSrcImage()), $dimensions['height'], _('Reported height and actual height of the source image don\'t match.'));
        }


        // assert invalid file types
        try {
            new GDProcessor(FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'cat.tmp');
            $this->assertTrue(false, _('Did not throw an exception with unsupported file type.' ));
        } catch(ImageCacheException $e) {
            $this->assertTrue(true);
        } catch (Exception $e) {
            $this->assertTrue(false, _('Did not throw ImageCacheException with unsupported file type.'));
        }

        try {
            new GDProcessor(FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'missing.jpg');
            $this->assertTrue(false, _('Did not throw an exception with non existing file.' ));
        } catch (ImageCacheException $e) {
            $this->assertTrue(true);
        } catch (Exception $e) {
            $this->assertTrue(false, _('Did not throw ImageCacheException with non existing file.'));
        }
    }


    public function testCropScale() {
        $processor = new GDProcessor($this->testImages[0]);
        $processor->cropScale(0, 0, 600, 399, 100, 150);

        $cacheImage = $processor->getCacheImage();
        $this->assertInternalType('resource', $cacheImage);

        $this->assertEquals(100, imagesx($cacheImage), _('The cached image has not the expected width.'));
        $this->assertEquals(150, imagesy($cacheImage), _('The cached image has not the expected height.'));
    }


    public function testSaveImage() {
        $processor = new GDProcessor($this->testImages[0]);
        $processor->cropScale(0, 0, 600, 399, 100, 150);
        $filename = FOLDER . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . '67845637485.jpg';
        $processor->saveImage($filename);

        /// test if the written image is the correct size
        $imageInfo = getimagesize($filename);
        $this->assertEquals(100, $imageInfo[0], _('Saved image has unexpected width.'));
        $this->assertEquals(150, $imageInfo[1], _('Saved image has unexpected height.'));

        unlink($filename);
    }
}