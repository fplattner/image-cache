<?php
/*
 * Copyright 2013 Florian Plattner http://florianplattner.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'lib/ImageProcessor.php';
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'lib/ImageCacheException.php';


class GDProcessor implements ImageProcessor {

    /** @var resource The source image in memory. */
    private $srcImage;
    /** @var resource The cached image in memory. */
    private $cacheImage;
    /** @var string The file type of the source image. */
    private $fileType;


    /**
     * constructs this ImageProcessor and loads the file into RAM
     *
     * @param $filename
     *
     * @throws ImageCacheException
     */
    public function __construct($filename) {
        if (file_exists($filename)) {
            $extension = strrchr($filename, '.');
            // if the extension is not recognized -> guess the file type
            if (!in_array($extension, array('.png', '.gif', '.jpg', '.jpeg'))) {
                $extension = $this->_guessExtension($filename);
            }

            switch ($extension) {
                case '.gif':
                    $this->fileType = 'gif';
                    $this->srcImage = imagecreatefromgif($filename);
                    break;

                case '.jpg':
                case '.jpeg':
                    $this->fileType = 'jpg';
                    $this->srcImage = imagecreatefromjpeg($filename);
                    break;

                case '.png':
                    $this->fileType = 'png';
                    $this->srcImage = imagecreatefrompng($filename);
                    imagealphablending($this->srcImage, false);
                    imagesavealpha($this->srcImage, true);
                    break;

                default:
                    throw new ImageCacheException(_('File type not supported.'));
            }
        } else {
            throw new ImageCacheException(sprintf(_('file %s does not exist.'), $filename));
        }
    }

    /**
     * frees all used resources.
     */
    public function __destruct() {
        if ($this->srcImage) {
            imagedestroy($this->srcImage);
        }

        if ($this->cacheImage) {
            imagedestroy($this->cacheImage);
        }
    }

    /**
     * crop $image to the size given by $srcX, $srcY, $srcWidth and $srcHeight and scale the result to $dstWidth, $dstHeight.
     *
     * @param $srcX      int   x coordinate of the top left corner of the crop region.
     * @param $srcY      int   y coordinate of the top left corner of the crop region.
     * @param $srcWidth  int   The width of the crop region
     * @param $srcHeight int   The height of the crop region
     * @param $dstWidth  int   The width to scale to.
     * @param $dstHeight int   The height to scale to.
     */
    public function cropScale($srcX, $srcY, $srcWidth, $srcHeight, $dstWidth, $dstHeight) {
        $this->cacheImage = $this->createCacheImage($dstWidth, $dstHeight);
        imagecopyresampled($this->cacheImage, $this->srcImage, 0, 0, $srcX, $srcY, $dstWidth, $dstHeight, $srcWidth, $srcHeight);
    }

    /**
     * get width and height of the image.
     *
     * @return array('width' => 123, 'height' => 456)
     */
    public function getImageDimensions() {
        return array('width' => imagesx($this->srcImage), 'height' => imagesy($this->srcImage));
    }

    /**
     * Saves the image as $this->fileType under $filename.
     *
     * @param $filename string The filename under wicht the image is to be saved.
     *
     * @return boolean true on success, false on failure
     */
    public function saveImage($filename) {
        if ($this->cacheImage) {
            switch ($this->fileType) {
                case 'gif':
                    return imagegif($this->cacheImage, $filename);
                    break;

                case 'jpg':
                    return imagejpeg($this->cacheImage, $filename);
                    break;

                case 'png':
                    return imagepng($this->cacheImage, $filename);
                    break;
            }
        }

        return false;
    }

    /**
     * This method can be implemented to do arbitrary filtering after the image has been cropped and scaled.
     * For example to add a watermark.
     *
     * @return mixed The filtered image.
     */
    public function filter() {
    }


    /**
     * @return resource
     */
    public function getCacheImage() {
        return $this->cacheImage;
    }

    /**
     * @return string
     */
    public function getFileType() {
        return $this->fileType;
    }

    /**
     * @return resource
     */
    public function getSrcImage() {
        return $this->srcImage;
    }

    /**
     * Creates a blank image in memory to be filled with the pixels for the cached image.
     *
     * @param $width
     * @param $height
     *
     * @return resource
     */
    protected function createCacheImage($width, $height) {
        $tmpImage = imagecreatetruecolor($width, $height);

        switch ($this->fileType) {
            case 'gif':
                $transparent = imagecolorallocatealpha($tmpImage, 0, 0, 0, 127);
                imagefilledrectangle($tmpImage, 0, 0, $width, $height, $transparent);
                imagecolordeallocate($tmpImage, $transparent);
                break;

            case 'png':
                imagesavealpha($tmpImage, true);
                imagealphablending($tmpImage, false);
                $transparent = imagecolorallocatealpha($tmpImage, 0, 0, 0, 127);
                imagefilledrectangle($tmpImage, 0, 0, $width, $height, $transparent);
                imagecolordeallocate($tmpImage, $transparent);
                break;

            case 'jpg':
                break;
        }

        return $tmpImage;
    }

    /**
     * Guesses the file type and the corresponding filename extension by inspecting the file on byte level.
     *
     * @param $filename
     *
     * @return string
     */
    protected function _guessExtension($filename) {
        $file   = @fopen($filename, 'r');
        $retVal = '';
        if ($file) {
            $content = fread($file, 10);
            if ($content) {
                if (substr($content, 0, 3) == 'GIF') {
                    $retVal = '.gif';
                } elseif (substr($content, 1, 3) == 'PNG') {
                    $retVal = '.png';
                } elseif (substr($content, 6, 4) == 'JFIF') {
                    $retVal = '.jpg';
                }
            }
            fclose($file);
        }
        return $retVal;
    }
}