<?php
/*
 * Copyright 2013 Florian Plattner http://florianplattner.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Implements image processing functionality.
 * It must keep track of the file type of the original image to reuse it upon save.
 */
interface ImageProcessor {

    /**
     * constructs this ImageProcessor and loads the file into RAM
     * @param $filename
     */
    public function __construct($filename);


    /**
     * crop $image to the size given by $srcX, $srcY, $srcWidth and $srcHeight and scale the result to $dstWidth, $dstHeight.
     *
     * @param $srcX      int   x coordinate of the top left corner of the crop region.
     * @param $srcY      int   y coordinate of the top left corner of the crop region.
     * @param $srcWidth  int   The width of the crop region
     * @param $srcHeight int   The height of the crop region
     * @param $dstWidth  int   The width to scale to.
     * @param $dstHeight int   The height to scale to.
     */
    public function cropScale($srcX, $srcY, $srcWidth, $srcHeight, $dstWidth, $dstHeight);


    /**
     * get width and height of the image.
     *
     * @return array('width' => 123, 'height' => 456)
     */
    public function getImageDimensions();


    /**
     * Saves the image as $fileType under $filename.
     *
     * @param $filename string The filename under wicht the image is to be saved.
     *
     * @return boolean true on success, false on failure.
     */
    public function saveImage($filename);


    /**
     * This method can be implemented to do arbitrary filtering after the image has been cropped and scaled.
     * For example to add a watermark.
     *
     * @return mixed The filtered image.
     */
    public function filter();
}