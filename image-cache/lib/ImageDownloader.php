<?php
/*
 * Copyright 2013 Florian Plattner http://florianplattner.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



interface ImageDownloader {

    /**
     * Downloads an image from $source and saves it in $target.
     *
     * @param $source string The source URL.
     * @param $target string The target filename.
     *
     * @return boolean true on success, false on failure.
     *
     * @throws ImageCacheException If the image cannot be downloaded.
     */
    public function download($source, $target);
}