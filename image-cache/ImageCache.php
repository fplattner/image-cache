<?php

/*
 * Copyright 2013 Florian Plattner http://florianplattner.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// TODO: encode image size in cache filename -> imagecache would deliver an image with the wrong dimensions if they are changed



require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib/ImageCacheException.php';

/**
 * creates cached versions of images. For example, when showing Feed-Contents on a website and the
 * images should not be downloaded on every page load, or the images should be displayed in a size or
 * format that is not supported by the source website.
 */
class ImageCache {

    /** keep proportions of the original image. Recalculate the height, keep the width. */
    const PROPORTION_KEEP_WIDTH    = 1;
    /** keep proportions of the original image. Recalculate the width, keep the height. */
    const PROPORTION_KEEP_HEIGHT   = 2;
    /** stretch the original image to fit the proportions of the cached version. */
    const PROPORTION_STRETCH       = 4;
    /** crop the biggest image out of the source image at a random position. */
    const PROPORTION_CROP_RANDOM   = 8;
    /** crop the biggest image that is centered (h and v) in the source image. */
    const PROPORTION_CROP_CENTERED = 16;

    /** @var string The folder to store cached images in. */
    private $cachePath;

    /** @var int The width of the images in cache. Source images get scaled to this width. */
    private $imageWidth;

    /** @var int The height of the images in cache. Source images get scaled to this height. */
    private $imageHeight;

    /**
     * @var int  Defines whether the source image's proportions are kept.
     *           Use the PROPORTIONS_* - constants to define.
     *           The default is PROPORTIONS_CROP.
     */
    private $calcProportion;

    /** @var string The name of the ImageProcessor class that does the image processing. */
    private $imageProcessorName;

    /** @var ImageDownloader The downloader that loads source images for processing. */
    private $imageDownloader;

    /** @var string The class name of the image downloader. */
    private $imageDownloaderName;

    /**
     * Constructor
     *
     * @param string          $cachePath The path where cached images are stored.
     * @see StreamDownloader
     */
    public function __construct($cachePath) {
        $this->setImageWidth(0);
        $this->setImageHeight(0);
        $this->setCalcProportion(self::PROPORTION_CROP_CENTERED);
        $this->setCachePath($cachePath);
        $this->setImageProcessorName('GDProcessor');
        $this->setImageDownloaderName('StreamDownloader');
    }

    /**
     * Returns the filename of the cached image depending on the URL of the original image.
     * If there is no image in cache, it is created.
     *
     * @param $url
     *
     * @return string
     */
    public function getCachedImagePath($url) {
        $filenameOnDisk = $this->createCacheFilename($url);

        if (!file_exists($filenameOnDisk)) {
            $this->cacheImage($url);
        }
        return $filenameOnDisk;
    }

    /**
     * Returns a relative URL to a cached image depending on the URL of the original image.
     * If there is no image in cache, it is created.
     *
     * @param $url
     *
     * @return string
     */
    public function getCachedImageUrl($url) {
        $filenameOnDisk = $this->createCacheFilename($url);
        if (!file_exists($filenameOnDisk)) {
            $this->cacheImage($url);
        }

        $cacheUrl = $this->createCacheUrl($url);
        return $cacheUrl;
    }

    /**
     * Clears the image defined by $url from cache. If $url is '', the whole cache is cleared.
     *
     * @param $url string The URL of the original image whose cached version should be deleted or '' to delete all cached images.
     *
     * @return boolean true if the image(s) could be deleted or false in case of failure.
     */
    public function clearCache($url = '') {
        $success = true;

        if ('' == $url) {
            $images = glob($this->getCachePath() . DIRECTORY_SEPARATOR . '*');
        } else {
            $images = array($this->createCacheFilename($url));
        }

        foreach ($images as $imagePath) {
            unlink($imagePath);
            if (file_exists($imagePath)) {
                $success = false;
            }
        }

        return $success;
    }

    /**
     * Create a cached version of the original image in $url.
     * @param $url string The URL of the original image.
     */
    public function cacheImage($url) {
        $tmpFilename = md5(uniqid());
        $cacheFilename = $this->createCacheFilename($url);
        $this->imageDownloader->download($url , $tmpFilename);
        $processorFilename = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'processors' . DIRECTORY_SEPARATOR . $this->getImageProcessorName() . '.php';
        if (file_exists($processorFilename)) {
            require_once $processorFilename;
        }
        /** @var $processor ImageProcessor */
        $processor = new $this->imageProcessorName($tmpFilename);
        $srcImageDimensions = $processor->getImageDimensions();
        $cacheImageDimensions = $this->getCachedImageDimensions($srcImageDimensions);
        $cropDimensions = $this->getCropDimensions($srcImageDimensions);
        $processor->cropScale($cropDimensions['left'],
                              $cropDimensions['top'],
                              $cropDimensions['width'],
                              $cropDimensions['height'],
                              $cacheImageDimensions['width'],
                              $cacheImageDimensions['height']);

        $processor->filter();
        $processor->saveImage($cacheFilename);
        unlink($tmpFilename);
    }

    /**
     * @param string $cachePath
     */
    public function setCachePath($cachePath) {
        $this->cachePath = '/' . trim($cachePath, '/ ');
    }

    /**
     * @return string
     */
    public function getCachePath() {
        return $this->cachePath;
    }

    /**
     * Set whether the source image's proportions are kept and how they are calculated.
     *
     * @param int $calcProportion One of:
     *                            ImageCache::PROPORTION_KEEP_WIDTH
     *                            ImageCache::PROPORTION_KEEP_HEIGHT
     *                            ImageCache::PROPORTION_STRETCH
     *                            ImageCache::PROPORTION_CROP_CENTERED
     *                            ImageCache::PROPORTION_CROP_RANDOM
     */
    public function setCalcProportion($calcProportion) {
        $this->calcProportion = $calcProportion;
    }

    /**
     * @return int
     */
    public function getCalcProportion() {
        return $this->calcProportion;
    }

    /**
     * @param int $imageHeight
     */
    public function setImageHeight($imageHeight) {
        $this->imageHeight = $imageHeight;
    }

    /**
     * @return int
     */
    public function getImageHeight() {
        return $this->imageHeight;
    }

    /**
     * @param int $imageWidth
     */
    public function setImageWidth($imageWidth) {
        $this->imageWidth = $imageWidth;
    }

    /**
     * @return int
     */
    public function getImageWidth() {
        return $this->imageWidth;
    }

    /**
     * @param string $imageDownloader
     */
    public function setImageDownloaderName($imageDownloader) {
        $this->imageDownloaderName = $imageDownloader;

        $downloaderPath = dirname(__FILE__) . DIRECTORY_SEPARATOR
                        . 'downloaders' . DIRECTORY_SEPARATOR
                        . $imageDownloader . '.php';

        require_once $downloaderPath;

        $this->imageDownloader = new $imageDownloader();
    }

    /**
     * @return string
     */
    public function getImageDownloaderName() {
        return $this->imageDownloaderName;
    }

    /**
     * @param string $imageProcessorName
     */
    public function setImageProcessorName($imageProcessorName) {
        $this->imageProcessorName = $imageProcessorName;
    }

    /**
     * @return string
     */
    public function getImageProcessorName() {
        return $this->imageProcessorName;
    }




    /**
     * Creates the filename on disk for an image to be cached.
     * @param $url string The path of the original image.
     *
     * @return string The path to the cached image.
     */
    protected function createCacheFilename($url) {
        // strip query string and fragment from the url
        $fragmentIndex = strrpos($url, '#');
        $queryIndex    = strrpos($url, '?');
        $stripIndex = -1;
        if ($fragmentIndex === false && $queryIndex !== false) {
            $stripIndex = $queryIndex;
        } elseif ($queryIndex === false && $fragmentIndex !== false) {
            $stripIndex = $fragmentIndex;
        } elseif ($fragmentIndex !== false && $queryIndex !== false) {
            $stripIndex = min($fragmentIndex, $queryIndex);
        }

        if ($stripIndex > -1) {
            $url = substr($url, 0, $stripIndex);
        }
        // extract extension
        $extension = strrchr($url, '.');
        // test if extension is a real extension and not a top level domain
        if (strpos($extension, '/')) {
            $extension = '';
        }

        $filename = md5($url);

        return $this->getCachePath() . DIRECTORY_SEPARATOR . $filename . $extension;
    }

    /**
     * Creates a relative URL where the cached images can be downloaded.
     * The cache folder must be somewhere in DocumentRoot, obviously.
     *
     * @param $url string The URL of the original image.
     *
     * @throws ImageCacheException if the URI cannot be created
     *
     * @return string A relative URL to the cached image.
     */
    protected function createCacheUrl($url) {
        $filenameOnDisk = $this->createCacheFilename($url);
        $urlPart = str_replace($_SERVER['DOCUMENT_ROOT'], '', $filenameOnDisk);

        if (strlen($urlPart) !== strlen($filenameOnDisk) - strlen($_SERVER['DOCUMENT_ROOT'])) {
            throw new ImageCacheException(sprintf(_('The URI for the cached version of the image in %s could not be computed.'), $url));
        }

        return '/' . trim($urlPart, '/ ');
    }

    /**
     * calculates the width and height of the cached image.
     *
     * @param $srcImageDimensions array The width and height of the original image.
     *
     * @return array('width' => 123, 'height' => 456)
     */
    protected function getCachedImageDimensions($srcImageDimensions) {
        $cacheWidth = $this->getImageWidth();
        $cacheHeight = $this->getImageHeight();
        $srcProportion = $srcImageDimensions['width'] / $srcImageDimensions['height'];


        if (0 == $cacheWidth && 0 == $cacheHeight) {
            $cacheWidth = $srcImageDimensions['width'];
            $cacheHeight = $srcImageDimensions['height'];
        } else {
            switch($this->getCalcProportion()) {
                case self::PROPORTION_CROP_RANDOM:
                case self::PROPORTION_CROP_CENTERED:
                    if (0 == $cacheWidth) {
                        $cacheWidth = round($cacheHeight * $srcProportion);
                    } elseif (0 == $cacheHeight) {
                        $cacheHeight = round($cacheWidth / $srcProportion);
                    }

                    break;

                case self::PROPORTION_KEEP_WIDTH:
                    if (0 == $cacheWidth) {
                        $cacheWidth = round($cacheHeight * $srcProportion);
                    } else {
                        // this covers both $cacheHeight == 0
                        //              and $cacheWidth != 0 && $cacheHeight != 0
                        $cacheHeight = round($cacheWidth / $srcProportion);
                    }
                    break;

                case self::PROPORTION_KEEP_HEIGHT:
                    if (0 == $cacheHeight) {
                        $cacheHeight = round($cacheWidth * $srcProportion);
                    } else {
                        // this covers both $cacheWidth == 0
                        //              and $cacheWidth != 0 && $cacheHeight != 0
                        $cacheWidth = round($cacheHeight * $srcProportion);
                    }
                    break;

                case self::PROPORTION_STRETCH:
                    if (0 == $cacheWidth) {
                        $cacheWidth = $srcImageDimensions['width'];
                    } elseif (0 == $cacheHeight) {
                        $cacheHeight = $srcImageDimensions['height'];
                    }
                    break;
            }
        }

        return array('width' => $cacheWidth, 'height' => $cacheHeight);
    }


    /**
     * Calculates the section of the original image used for the cropped version.
     *
     * @param $srcImageDimensions    array The width and height of the original image.
     *
     * @return array('top' => 123, 'left' => 456, 'width' => 78, 'height' => 90)
     */
    protected function getCropDimensions($srcImageDimensions) {
        $cachedImageDimensions = $this->getCachedImageDimensions($srcImageDimensions);
        $retVal = array('top' => 0, 'left' => 0, 'width' => $srcImageDimensions['width'], 'height' => $srcImageDimensions['height']);

        switch ($this->getCalcProportion()) {
            case self::PROPORTION_CROP_CENTERED:
                $srcProportion   = $srcImageDimensions['width'] / $srcImageDimensions['height'];
                $cacheProportion = $cachedImageDimensions['width'] / $cachedImageDimensions['height'];
                if ($srcProportion < $cacheProportion) { // src image is taller than cache image
                    $retVal['left']   = 0;
                    $retVal['height'] = round($srcImageDimensions['width'] / $cacheProportion);
                    $retVal['top']    = round(($srcImageDimensions['height'] - $retVal['height']) / 2);
                } elseif ($srcProportion > $cacheProportion) { // src image is wider than cache image
                    $retVal['top']   = 0;
                    $retVal['width'] = round($srcImageDimensions['height'] * $cacheProportion);
                    $retVal['left']  = round(($srcImageDimensions['width'] - $retVal['width']) / 2);
                }
                break;

            case self::PROPORTION_CROP_RANDOM:
                $srcProportion   = $srcImageDimensions['width'] / $srcImageDimensions['height'];
                $cacheProportion = $cachedImageDimensions['width'] / $cachedImageDimensions['height'];
                if ($srcProportion < $cacheProportion) { // src image is taller than cache image
                    $retVal['left']   = 0;
                    $retVal['height'] = round($srcImageDimensions['width'] / $cacheProportion);
                    $retVal['top']    = round((mt_rand() / mt_getrandmax()) * (($srcImageDimensions['height'] - $retVal['height']) / 2));
                } elseif ($srcProportion > $cacheProportion) { // src image is wider than cache image
                    $retVal['top']  = 0;
                    $retVal['width'] = round($srcImageDimensions['height'] * $cacheProportion);
                    $retVal['left'] = round((mt_rand() / mt_getrandmax()) * (($srcImageDimensions['width'] - $retVal['width']) / 2));
                }

                break;
        }

        return $retVal;
    }
}